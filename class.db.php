<?php
class db extends PDO {
	private $error;
	private $sql;
	private $bind;
    private $inputData;
	private $errorCallbackFunction;
	private $errorMsgFormat;
    private $dumpQuery;

	public function __construct($dsn, $user="", $passwd="", $options=array()) {
        $dumpQuery = false;
        if(empty($options)){
            $options = array(
                PDO::ATTR_PERSISTENT => false,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            );
        }

		try {
			parent::__construct($dsn, $user, $passwd, $options);
		} catch (PDOException $e) {
                trigger_error($e->getMessage());
                return false;
		}
	}

    private function checkBindArray($bind){
        if(!is_array($bind)) {
            if(!empty($bind)) {
                $bind = array($bind);
            } else {
                $bind = array();
            }
        }
        return $bind;
    }

    private function cleanup($sql, $bind) {
        //Possible bind params: $bind = 'val'; $bind = array('val', ':key' => 'val, ':key' => $valArray, ':key' => array('val', 'val');
        $this->sql = $sql;
        $this->bind = $bind;

        $bind = $this->checkBindArray($bind);

        $bindNamed = array();
        $bindQuestion = array();

        foreach($bind as $bindKey => $bindValue){
            if(strstr($bindKey, ":")){
                $bindNamed[$bindKey] = $bindValue;
            } else {
                $bindQuestion[] = $bindValue;
            }
        }

        $newBindParams = array();
        $questionArrayCount = 0;
        preg_match_all('/(?<question>\?)|(?<named>\:\w*)/', $sql, $queryParams);
        $namedParams = array_filter($queryParams['named']);
        $questionParams = array_filter($queryParams['question']);
        if(array_diff(array_keys($bindNamed), $namedParams)){
            $this->error = 'You have defined more named params than are present in your query.';
            $this->debug();
            return false;
        } else if(count($bindQuestion) > count($questionParams)){
            $this->error = 'You have defined more non-named params than are present in your query.';
            $this->debug();
            return false;
        }
        foreach ($queryParams[0] as $param) {
            if (strstr($param, ":")) {
                if (array_key_exists($param, $bindNamed)) {
                    if (is_array($bindNamed[$param])) {
                        $replaceInQuery = implode(',', array_fill(0, count($bindNamed[$param]), '?'));
                        foreach ($bindNamed[$param] as $innerValue) {
                            $newBindParams[] = $innerValue;
                        }
                    } else {
                        $replaceInQuery = '?';
                        $newBindParams[] = $bindNamed[$param];
                    }
                    $sql = str_replace($param, $replaceInQuery, $sql);
                }
            } else {
                if(array_key_exists($questionArrayCount, $bindQuestion)){
                    if (!is_array($bindQuestion[$questionArrayCount])) {
                        $newBindParams[] = $bindQuestion[$questionArrayCount];
                        $questionArrayCount++;
                    } else {
                        $this->error = 'You cannot pass in an array for a value without a named param.';
                        $this->debug();
                        return false;
                    }
                } else {
                    $this->error = 'You are missing a non-named param. Param #'. ($questionArrayCount+1);
                    $this->debug();
                    return false;
                }
            }
        }
        foreach($newBindParams as $key => $val) {
            $newBindParams[$key] = stripslashes($val);
        }
        if(preg_match_all('/(?<question>\?)|(?<named>\:\w*)/', $sql, $queryParams)){
            $namedParams = array_filter($queryParams['named']);
            $questionParams = array_filter($queryParams['question']);
            if(!empty($namedParams)) {
                $this->sql = $sql;
                $this->bind = $newBindParams;
                $this->error = 'You have named elements in your query that do not have a matching bind param. '. implode(", ", $namedParams);
                $this->debug();
                return false;

            } else if(count($questionParams) != count($newBindParams)){
                $this->error = 'The number of bind params does not match the number of query replacements.';
                $this->debug();
                return false;
            }
        }
        $this->sql = trim($sql);
        $this->bind = $newBindParams;
    }

	private function debug() {
		if(!empty($this->errorCallbackFunction)) {
			$error = array("Error" => $this->error);
			if(!empty($this->sql))
				$error["SQL Statement"] = $this->sql;
			if(!empty($this->bind))
				$error["Bind Parameters"] = trim(print_r($this->bind, true));
            if(!empty($this->inputData))
                $error["Input Data"] = trim(print_r($this->inputData, true));

			$backtrace = debug_backtrace();
			if(!empty($backtrace)) {
				foreach($backtrace as $info) {
					if(isset($info["file"] ) && $info["file"] != __FILE__)
						$error["Backtrace"] = $info["file"] . " at line " . $info["line"];	
				}		
			}

			$msg = "";
			if($this->errorMsgFormat == "html") {
				if(!empty($error["Bind Parameters"]))
					$error["Bind Parameters"] = "<pre>" . $error["Bind Parameters"] . "</pre>";
                if(!empty($error["Input Data"]))
                    $error["Input Data"] = "<pre>" . $error["Input Data"] . "</pre>";
				$css = trim(file_get_contents(dirname(__FILE__) . "/error.css"));
				$msg .= '<style type="text/css">' . "\n" . $css . "\n</style>";
				$msg .= "\n" . '<div class="db-error">' . "\n\t<h3>SQL Error</h3>";
				foreach($error as $key => $val)
					$msg .= "\n\t<label>" . $key . ":</label>" . $val;
				$msg .= "\n\t</div>\n</div>";
			}
			elseif($this->errorMsgFormat == "text") {
				$msg .= "SQL Error\n" . str_repeat("-", 50);
				foreach($error as $key => $val)
					$msg .= "\n\n$key:\n$val";
			}

			$func = $this->errorCallbackFunction;
			$func($msg);
		}
	}

	public function delete($table, $where, $bind="") {
		$sql = "DELETE FROM " . $table . " WHERE " . $where . ";";
        return $this->run($sql, $bind);
	}

    public function dump($status=true) {
        if($status){
            $this->dumpQuery = true;
        } else {
            $this->dumpQuery = false;
        }
    }

	private function filter($table, $info) {
		$driver = $this->getAttribute(PDO::ATTR_DRIVER_NAME);
		if($driver == 'sqlite') {
			$sql = "PRAGMA table_info('" . $table . "');";
			$key = "name";
		}
		elseif($driver == 'mysql') {
			$sql = "DESCRIBE " . $table . ";";
			$key = "Field";
		}
		else {	
			$sql = "SELECT column_name FROM information_schema.columns WHERE table_name = '" . $table . "';";
			$key = "column_name";
		}	
        $returnArray = array();

        $prevDumpQuery = $this->dumpQuery;
        $this->dumpQuery = false;
		if(false !== ($list = $this->run($sql))) {
			$fields = array();
			foreach($list as $record)
				$fields[] = $record[$key];

            $returnArray = array_values(array_intersect($fields, array_keys($info)));
		}
        $this->dumpQuery = $prevDumpQuery;
		return $returnArray;
	}

	public function insert($table, $inputData) {
		$fields = $this->filter($table, $inputData);
		$sql = "INSERT INTO " . $table . " (" . implode(", ", $fields) . ") VALUES (:" . implode(", :", $fields) . ");";
		$bind = array();
		foreach($fields as $field)
			$bind[":$field"] = $inputData[$field];

        if(empty($bind) || array_diff(array_keys($inputData), $fields)){
            $this->sql = $sql;
            $this->bind = $bind;
            $this->inputData = $inputData;
            $this->error = 'Some of the field(s) provided to the insert cannot be found in the table. Check your input data array.';
            $this->debug();
            return false;
        }

        return $this->run($sql, $bind);
	}

    private function outputRenderedQuery($sql, $bind="") {
        $sql = $this->sql;
        foreach($this->bind as $param){
            $sql = substr_replace($sql, "'". $param ."'", strpos($sql, '?'), 1);
        }
        return $sql;
    }

	public function run($sql, $bind="") {
        if($this->cleanup($sql, $bind) === false){
            return false;
        }
		$this->error = "";

        if($this->dumpQuery){
            return $this->outputRenderedQuery($sql, $bind);
        }

		try {
			$pdostmt = $this->prepare($this->sql);
			if($pdostmt->execute($this->bind) !== false) {
				if(preg_match("/^(" . implode("|", array("select", "describe", "pragma")) . ")/i", $this->sql)){
                    return $pdostmt->fetchAll(PDO::FETCH_ASSOC);
                } elseif(preg_match("/^(" . implode("|", array("delete", "insert", "update")) . ")/i", $this->sql)) {
                    return $pdostmt->rowCount();
                } else {
                    //TODO Maybe add some output here if another type of query is run.
                    return true;
                }
			}	
		} catch (PDOException $e) {
			$this->error = $e->getMessage();
			$this->debug();
			return false;
		}
	}

	public function select($table, $where="", $bind="", $fields="*") {
		$sql = "SELECT " . $fields . " FROM " . $table;
		if(!empty($where))
			$sql .= " WHERE " . $where;
		$sql .= ";";

        return $this->run($sql, $bind);
	}

	public function setErrorCallbackFunction($errorCallbackFunction, $errorMsgFormat="html") {
		//Variable functions for won't work with language constructs such as echo and print, so these are replaced with print_r.
		if(in_array(strtolower($errorCallbackFunction), array("echo", "print")))
			$errorCallbackFunction = "print_r";

		if(function_exists($errorCallbackFunction)) {
			$this->errorCallbackFunction = $errorCallbackFunction;	
			if(!in_array(strtolower($errorMsgFormat), array("html", "text")))
				$errorMsgFormat = "html";
			$this->errorMsgFormat = $errorMsgFormat;	
		}	
	}

	public function update($table, $inputData, $where, $bind="") {
        $bind = $this->checkBindArray($bind);
		$fields = $this->filter($table, $inputData);
		$fieldSize = sizeof($fields);

		$sql = "UPDATE " . $table . " SET ";
		for($f = 0; $f < $fieldSize; ++$f) {
			if($f > 0)
				$sql .= ", ";
			$sql .= $fields[$f] . " = :pdowrap_" . $fields[$f];
		}
		$sql .= " WHERE " . $where . ";";

		foreach($fields as $field)
			$filterbind[":pdowrap_$field"] = $inputData[$field];

        if(empty($filterbind) || array_diff(array_keys($inputData), $fields)){
            $this->sql = $sql;
            $this->bind = $bind;
            $this->inputData = $inputData;
            $this->error = 'Some of the field(s) provided to the update cannot be found in the table. Check your input data array.';
            $this->debug();
            return false;
        }

        $bind = array_merge($filterbind, $bind);

        return $this->run($sql, $bind);
	}
}	
?>
