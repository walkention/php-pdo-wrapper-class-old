<?php
require_once ('util/php-pdo-wrapper-class/class.db.php');

class db_connect extends db {
    private $aDBConnectVars;

    public function __construct($aUseDbConnection = 'APP_WEBSITE', $aConnectOptions=array()) {
        $aConnectionInfo = $this->parse_db_login_constant($aUseDbConnection);
        $aConnectionInfo['options'] = $aConnectOptions;
        if (!empty($aConnectionInfo)) {
            $this->aDBConnectVars = $aConnectionInfo;
            $oDBConn = $this->connect();
            return $oDBConn;
        } else {
            return false;
        }
    }

    public function change_db($aUseDbConnection = 'APP_WEBSITE', $aConnectOptions=array()) {
        return $this->__construct($aUseDbConnection, $aConnectOptions);
    }

    private function connect() {
        $aDbVars = $this->aDBConnectVars;
        if(isset($aDbVars['type'])){
            switch ($aDbVars['type']){
                case 'sqlite':
                    $oDBConnection = parent::__construct("sqlite:" .$aDbVars['db']);
                    break;
                case 'postgres':
                    $oDBConnection = parent::__construct("pgsql:host=". $aDbVars['host'] .";dbname=". $aDbVars['db'], $aDbVars['user'], $aDbVars['password'], $aDbVars['options']);
                    break;
                default:
                    $oDBConnection = parent::__construct("mysql:host=". $aDbVars['host'] .";dbname=". $aDbVars['db'], $aDbVars['user'], $aDbVars['password'], $aDbVars['options']);
                    break;
            }

        } else {
            $oDBConnection = parent::__construct("mysql:host=". $aDbVars['host'] .";dbname=". $aDbVars['db'], $aDbVars['user'], $aDbVars['password'], $aDbVars['options']);
        }
        return $oDBConnection;
    }

    private function parse_db_login_constant($sName) {
        if (defined($sName)) {
            if (($aArr = unserialize(constant($sName))) !== false) {
                return $aArr;
            } else {
                return constant($sName);
            }
        } else {
            return null;
        }
    }

}

?>